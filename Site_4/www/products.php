<?php ?>
    <h4>Products</h4>
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Товар</th>
                    <th>Цена</th>
                    <th>Описание</th>
                </tr>
            </thead>
            <tbody>
                <?
                $obj = json_decode(file_get_contents("./ajax/products.json"));
                $str = "";
                foreach ($obj as $it) {
                    $str .= "<tr>\n";        
                    $str .= "<td>" . $it->{"name"} . "</td>\n<td>" . $it->{"cost"} . "</td>\n<td>" . $it->{"description"} . "</td>\n"; 
                    $str .= "</tr>";     
                }
                echo ($str);
            ?>
            </tbody>
        </table>
    </div>
