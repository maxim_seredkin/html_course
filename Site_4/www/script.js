$(document).ready(function () {
    $('#left').css({
        height: $('.up-layer').height()
    });
    $('#right').css({
        height: $('.up-layer').height()
    });
    $('#left').click(function (event) {
        $('.up-layer').hide();
        $('.about-product>h5').detach();
    });
    $('#right').click(function (event) {
        $('.up-layer').hide();
        $('.about-product>h5').detach();
    });
});

function GetTable() {
    $.ajax({
        dateType: 'json',
        type: 'POST',
        url: 'products.php',
        success: function (data) {
            $("#product_table").html(data.toString());
            $('tbody>tr').each(function () {
                $(this).dblclick(tr_dblclick);
                $(this).click(tr_click);
            });
            $('#div_bttnGetInfo').slideDown("slow");
            $('#bttnGetTable').attr('value', 'Refresh');
        }
    });
}

function tr_dblclick(handler) {
    GetTheProduct($('tr.active>td').first().html());
}

function tr_click(handler) {
    $('tbody>tr').removeClass('active');
    $(this).addClass('active');
}

function bttnGetInfo_click() {
    GetTheProduct($('tr.active>td').first().html());
}

function GetTheProduct(name) {
    $.ajax({
        dateType: 'json',
        type: 'POST',
        url: 'product.php',
        data: 'name=' + name,
        beforeSend: function () {
            $('.up-layer').show("slow");
            $('#info').hide();
            $('.preloader').show();
        },
        success: function (data) {
            var json_info = JSON.parse(data);
            $('#title_product').html('About product: ' + json_info['name']);
            $('#tbName').val(json_info['name']);
            $('#tbCost').val(json_info['cost']);
            $('#tbDescription').html(json_info['description']);
            setTimeout(function () {
                $('.preloader').hide();
                $('#info').show();
            }, 5000);
        },
        error: function () {
            $('.preloader').hide();
            $('.about-product').append('<h5 style="color:red; text-align:center;">Не удается загрузить данные!<h5>');
        },
        timeout: 10000
    });
}
